import Logger from '../server/logger';
import populate from './populate';

const NUMBER_OF_USERS_TO_POPULATE = 20;

(async () => {
  try {
    await populate(false, NUMBER_OF_USERS_TO_POPULATE);
  } catch (e) {
    Logger.log(e);
  }
})();
