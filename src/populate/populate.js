import fs from 'fs';

import DBInterface from '../server/DBInterface';
import Collections from '../server/collections/collections';
import SERVERROR from '../server/errcodes';

const PATH_TO_ENV_FILE = '../env.json';

// Get content from file
const contents = fs.readFileSync(PATH_TO_ENV_FILE);

// Define to JSON type
const jsonContent = JSON.parse(contents);

if (!jsonContent) throw new Error(SERVERROR.E0100);

if (!jsonContent.db) throw new Error(SERVERROR.E0101);

const {
  db,
} = jsonContent;

module.exports = async (admin, NUMBER_OF_USERS_TO_POPULATE) => {
  await DBInterface.getInstance(db).connect();

  const users = new Array(NUMBER_OF_USERS_TO_POPULATE);

  for (let i = 0; i < users.length; i += 1) {
    const rString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

    users[i] = {
      firstname: rString,

      lastname: rString,

      login: rString,

      email: `${rString}@${rString}.com`,

      passwd: rString,

      admin,

      color: `#${Math.floor(Math.random() * 16777215).toString(16)}`,
    };
  }

  await Collections.getCollection('User').create(users);

  await DBInterface.getInstance(db).close();
};
