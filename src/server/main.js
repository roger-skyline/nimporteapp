import fs from 'fs';

import DBInterface from './DBInterface';
import Mailer from './mail';
import SERVERROR from './errcodes';
import Logger from './logger';
import Server from './server';

import User from './collections/user';

let instance = null;

/*
 * Close properly the connection on signal
 */
process.on('SIGINT', async () => {
  Logger.log('SIGINT');

  try {
    if (DBInterface.hasInstance()) {
      await DBInterface.getInstance().close();
    }
  } catch (e) {
    // Exit anyway
  }

  process.exit();
});

class System {
  constructor() {
    this.db = false;
    this.server = false;
  }

  static get PATH_TO_ENV_FILE() {
    return '../env.json';
  }

  static getInstance() {
    if (!instance) {
      instance = new System();
    }

    return instance;
  }

  configDB() {
    [{
        key: 'host',
        default: 'localhost',
      },
      {
        key: 'port',
        default: 27017,
      },
      {
        key: 'name',
        default: 'admin',
      },
      {
        key: 'username',
        default: 'passwd',
      },
      {
        key: 'password',
        default: 'passwd',
      },
      {
        key: 'sslKey',
        default: '',
      },
      {
        key: 'sslCert',
        default: '',
      },
    ].forEach((x) => {
      if (!this.db[x.key] || this.db[x.key] === '') this.db[x.key] = x.default;
    });
  }

  configure() {
    try {
      // Get content from file
      const contents = fs.readFileSync(System.PATH_TO_ENV_FILE);

      // Define to JSON type
      const jsonContent = JSON.parse(contents);

      if (!jsonContent) throw new Error(SERVERROR.E0100);

      if (!jsonContent.db) throw new Error(SERVERROR.E0101);
      this.db = jsonContent.db;

      if (!jsonContent.server) throw new Error(SERVERROR.E0102);
      this.server = jsonContent.server;

      if (!jsonContent.mail) throw new Error(SERVERROR.E0102);
      this.mail = jsonContent.mail;

      Mailer.getInstance(this.mail);

      this.configDB(jsonContent.db);

      // Set default port to 8080
      if (!this.server.port || this.server.port === '') this.server.port = 8080;
    } catch (e) {
      Logger.logerr(e.toString());
      process.exit();
    }
  }

  async boot() {
    this.configure();

    try {
      await DBInterface.getInstance(this.db).connect();

      const server = new Server(this.server);

      // Ensure admin user exist
      await User.methods.ensureAdmin();

      server.listen();
    } catch (e) {
      Logger.logerr(e);
      process.exit();
    }
  }
}

System.getInstance().boot();
