import Logger from './logger';

const exec = require('child_process').exec;

let instance = null;

export default class Mailer {
  constructor(mailInfos) {
    this.user = mailInfos.user;
    this.password = mailInfos.password;
    this.host = mailInfos.host;
    this.port = mailInfos.port;
    this.from = mailInfos.from;
		console.log('mailInfos', mailInfos);
  }

  static sendmail(mailOptions) {
    const {
      text,
      subject,
      from,
      to,
    } = mailOptions;

    const qwe = `echo '${text}'| mail -s '${subject}' -r '${from}' '${to}'`;

    const mail = exec(qwe, (err, stdout, stderr) => {
      if (err) {
        Logger.logerr('Mailer error : ', err, stderr);
      }

      Logger.log('Mail sent : ', stdout);
    });

    mail.on('exit', Mailer.then);
  }

  static getInstance(mailInfos) {
    if (instance === null) {
      instance = new Mailer(mailInfos);
    }

    return instance;
  }

  static hasInstance() {
    return !!instance;
  }

  connection(email, ip, date) {
    const text = `You just connected to Roger at ${date} from ${ip} !`;

    // setup email data with unicode symbols
    const mailOptions = {
      // sender address
      from: this.from,

      // list of receivers 'a@mail.com b@mail.com ...
      to: email,

      // Subject line
      subject: 'New Connection ✔',

      // plain text body
      text,

      // html body
      html: `<b>${text}</b>`,
    };

    // send mail with defined transport object
    Mailer.sendmail(mailOptions);
  }


  creation(email) {
    const text = 'Roger has created an account for you !';

    // setup email data with unicode symbols
    const mailOptions = {
      // sender address
      from: this.from,

      // list of receivers
      to: email,

      // Subject line
      subject: 'New Creation Account ✔',

      // plain text body
      text,

      // html body
      html: `<b>${text}</b>`,
    };

    // send mail with defined transport object
    Mailer.sendmail(mailOptions);
  }

  update(email) {
    const text = 'Your account has been updated!';

    // setup email data with unicode symbols
    const mailOptions = {
      // sender address
      from: this.from,

      // list of receivers
      to: email,

      // Subject line
      subject: 'Account update ✔',

      // plain text body
      text,

      // html body
      html: `<b>${text}</b>`,
    };

    // send mail with defined transport object
    Mailer.sendmail(mailOptions);
  }

  delete(email) {
    const text = 'Your account has been delete!';

    // setup email data with unicode symbols
    const mailOptions = {
      // sender address
      from: this.from,

      // list of receivers
      to: email,

      // Subject line
      subject: 'Account deletion ✔',

      // plain text body
      text,

      // html body
      html: `<b>${text}</b>`,
    };

    // send mail with defined transport object
    Mailer.sendmail(mailOptions);
  }

  static then(error, info) {
    return true;
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
  }

  isConnected() {
    return this.connectionVar;
  }
}
