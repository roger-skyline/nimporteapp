import express from 'express';
import session from 'express-session';
import bodyParser from 'body-parser';
import https from 'https';
import fs from 'fs';

import Logger from './logger';

import authenticated from './middlewares/authenticated';
import isAdmin from './middlewares/isAdmin';

import login from './api/login';
import add from './api/add';
import update from './api/update';
import del from './api/delete';
import logout from './api/logout';

import deleteView from './views/delete';
import updateView from './views/update';
import indexView from './views/index';
import homeView from './views/home';

export default class Server {
  constructor(server) {
    this.app = express();

    this.app.use(session({
      secret: server.session.secret,

      resave: true,

      saveUninitialized: false,

      cookie: {
        maxAge: 60000,
      },
    }));

    const {
      port,
      sslKey,
      sslCert,
    } = server;

    this.port = port;
    this.sslKey = sslKey;
    this.sslCert = sslCert;

    // Support SSL
    this.ssl = {
      key: fs.readFileSync(this.sslKey, 'utf8'),
      cert: fs.readFileSync(this.sslCert, 'utf8'),
    };

    // Support json encoded bodies
    this.app.use(bodyParser.json());

    // Support encoded bodies
    this.app.use(bodyParser.urlencoded({
      extended: false,
    }));

    this.https = https.createServer(this.ssl, this.app);

    this.registerViews();

    this.registerApi();

    this.registerDefault();
  }

  /**
   * This function registers all functionnalities of the api
   */
  registerApi() {
    const mapApi = [{
        route: '/login',
        api: login,
      },
      {
        route: '/add',
        api: add,
        middleware: isAdmin,
      },
      {
        route: '/update',
        api: update,
        middleware: isAdmin,
      },
      {
        route: '/delete',
        api: del,
        middleware: isAdmin,
      },
      {
        route: '/logout',
        api: logout,
        middleware: authenticated,
      },
    ];

    // use res.render to load up an ejs view file
    for (let i = 0; i < mapApi.length; i += 1) {
      if (mapApi[i].middleware) this.app.post(mapApi[i].route, mapApi[i].middleware, mapApi[i].api);
      else this.app.post(mapApi[i].route, mapApi[i].api);
    }
  }

  /**
   * This function reister all views to the app
   * Its only setting render for specific get route
   */
  registerViews() {
    const mapRouteToRender = [{
        route: '/add',
        render: (req, res) => res.render('add', req.session.client),
        middleware: isAdmin,
      },
      {
        route: '/delete',
        render: deleteView,
        middleware: isAdmin,
      },
      {
        route: '/home',
        render: homeView,
        middleware: authenticated,
      },
      {
        route: '/',
        render: indexView,
      },
      {
        route: '/user',
        middleware: isAdmin,
        render: updateView,
      },
      {
        route: '/logout',
        render: logout,
      },
      {
        route: '/login',
        render: (req, res) => res.render('login'),
      },
      {
        route: '/404',
        render: (req, res) => res.render('404'),
      },
      {
        route: '/504',
        render: (req, res) => res.render('504'),
      },
    ];

    // set the view engine to ejs
    this.app.set('view engine', 'ejs');

    // use res.render to load up an ejs view file
    for (let i = 0; i < mapRouteToRender.length; i += 1) {
      if (mapRouteToRender[i].middleware) this.app.get(mapRouteToRender[i].route, mapRouteToRender[i].middleware, mapRouteToRender[i].render);
      else this.app.get(mapRouteToRender[i].route, mapRouteToRender[i].render);
    }
  }

  registerDefault() {
    this.app.use((req, res) => {
      res.status(404);

      // respond with html page
      if (req.accepts('html')) {
        return res.render('404', {
          url: req.url,
        });
      }

      // respond with json
      if (req.accepts('json')) {
        return res.send({
          error: 'Not found.',
        });
      }

      // default to plain-text. send()
      return res.type('txt').send('Not found.');
    });
  }

  listen() {
    this.server = this.https.listen(this.port, () => Logger.log(`Server listening on port ${this.port}`));

    this.server.on('error', err =>
      Logger.logerr(`Server HTTPS failed at port ${this.port} => ${String((err && err.stack) || err)}`));
  }
}
