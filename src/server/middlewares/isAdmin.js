import Logger from '../logger';
import Collections from '../collections/collections';

module.exports = async (req, res, next) => {
  const {
    client,
  } = req.session;

  try {
    if (!client) return res.redirect('/login');

    if (!client || !client.isAdmin) {
      const err = new Error('Not admin.');

      err.status = 401;

      return next(err);
    }

    return next();
  } catch (e) {
    Logger.logerr(e);

    return next(e);
  }
};
