module.exports = async (req, res, next) => {
  const {
    client,
  } = req.session;

  if (!client) {
    return res.redirect('/login');
  }

  return next();
};
