export default class Logger {
  static log(...msg) {
    console.log(`[${new Date().toUTCString()}] : `, ...msg);
  }

  static logerr(...msg) {
    console.log(`[${new Date().toUTCString()}] : `, ...msg);
    console.error(`[${new Date().toUTCString()}] : `, ...msg);
  }
}
