import mongoose from 'mongoose';
import fs from 'fs';

import Logger from './logger';

import Collections from './collections/collections';

let instance = null;

/**
 * This is the class to use to interact with MongoDB
 */
export default class DBInterface {
  constructor(dbInfos) {
    this.db = false;

    mongoose.Promise = Promise;

    this.connectionVar = false;

    // Create the url to connect the database
    const auth = `${dbInfos.username}:${dbInfos.password}@`;

    this.name = dbInfos.name;
    this.host = dbInfos.host;
    this.port = dbInfos.port;
    this.username = dbInfos.username;
    this.password = dbInfos.password;
    this.sslKey = dbInfos.sslKey;
    this.sslCert = dbInfos.sslCert;
    this.sslCa = dbInfos.sslCa;

    // Use auth or not depending on the data we have
    this.uri = `mongodb://${auth}${dbInfos.host}:${dbInfos.port}/${dbInfos.name}?ssl=true`;
    console.log(this.uri);
  }

  static getInstance(dbInfos) {
    if (instance === null) {
      instance = new DBInterface(dbInfos);
    }

    return instance;
  }

  static hasInstance() {
    return !!instance;
  }

  isConnected() {
    return this.connectionVar;
  }

  close() {
    mongoose.disconnect();

    this.db.close();
  }

  /**
   * Build the authentification data we need to connect the database
   */
  options() {
    return {
      // We disable reconnect from mongoose
      auto_reconnect: false,

      // For performances consideraétion, mongoose open multiple mongodb connections to handle one
      poolSize: 5,

      // For long running applications it is often prudent to enable keepAlive.
      // Without it, after some period of time you may start to see "connection closed" errors for what seems like no reason.
      // From mongoose documentation
      keepAlive: true,

      autoIndex: true,

      socketTimeoutMS: 30000,
      connectTimeoutMS: 120000,

      auth: {
        authdb: 'admin',
      },

      server: {
        sslValidate: false,
        sslKey: fs.readFileSync(this.sslKey),
        sslCert: fs.readFileSync(this.sslCert),
        sslCa: fs.readFileSync(this.sslCa),
      },

      // Set the user for authentification
      user: this.username,

      // Set the password for authentification
      pass: this.password,
    };
  }

  connect() {
    const db = mongoose.createConnection(this.uri, this.options());

    this.db = db;

    return new Promise((resolve, reject) => {
      db.once('open', () => {
        this.db.removeAllListeners('error');

        Collections.loadCollections(this.db);

        return resolve(this);
      });

      db.once('error', (err) => {
        Logger.logerr(`Mongodb connection failed at ${this.uri}`, err);

        return reject(err);
      });

      db.once('disconnected', (msg) => {
        Logger.log(this.connectionVar ?
          'Mongodb connection is OFF' :
          `Mongodb connection failed at ${this.uri}`);

        this.connectionVar = false;

        if (msg) Logger.log(msg);
      });

      db.once('connected', () => {
        this.connectionVar = true;

        Logger.log(`Mongodb connection is ON at ${this.uri}`);
      });

      db.once('close', (msg) => {
        this.connectionVar = false;

        Logger.log('Mongodb connection is CLOSED');

        this.db = false;

        if (msg) Logger.log(msg);
      });
    });
  }
}
