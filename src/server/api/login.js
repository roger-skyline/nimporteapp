import Collections from '../collections/collections';
import User from '../collections/user';
import Logger from '../logger';
import Mail from '../mail';

/**
 * Handle a user login
 */
module.exports = async (req, res) => {
  try {
    const user = await Collections.getCollection('User').findOne({
      login: String(req.body.login),
    });

    if (!user) return res.redirect('/login');

    const pwd = User.methods.crypto(req.body.passwd, user.salt);

    // Check that the provided password matches
    if (pwd !== user.passwd) return res.redirect('/login');

    setTimeout(() =>
      Mail.getInstance().connection(user.email, req.headers['x-forwarded-for'] || req.connection.remoteAddress, new Date()), 0);

    const {
      session,
    } = req;

    session.client = {
      login: user.login,
      color: user.color,
      isAdmin: user.admin,
    };

    return res.redirect('/');
  } catch (e) {
    Logger.logerr(e);

    return res.redirect('/504');
  }
};
