 import Logger from '../logger';

 /**
  * Handle a user logout from session
  */
 module.exports = async (req, res) => {
   try {
     if (req.session) {
       // delete session object
       await req.session.destroy();

       res.redirect('/');
     }
   } catch (e) {
     Logger.logerr(e);

     res.redirect('/504');
   }
 };
