import Collections from '../collections/collections';
import Logger from '../logger';
import Mail from '../mail';

/**
 * Handle a user login
 */
module.exports = async (req, res) => {
  try {
    const {
      users,
    } = req.body;

    if (!users) {
      res.redirect('/');
    }

    const usersDeleted = await Collections.getCollection('User').find({
      login: users,
    });

    await Collections.getCollection('User').remove({
      login: users,
    });

    console.log('usersDeleted', usersDeleted);

    usersDeleted.forEach(x =>
      Mail.getInstance().delete(x.email, new Date()));


    return res.redirect('/');
  } catch (e) {
    Logger.logerr(e);

    return res.redirect('/504');
  }
};
