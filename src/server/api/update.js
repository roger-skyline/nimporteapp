import Collections from '../collections/collections';
import Logger from '../logger';
import Mail from '../mail';
import User from '../collections/user';

/**
 * Handle a user login
 */
module.exports = async (req, res) => {
  try {
    const {
      login,
    } = req.query;

    const {
      newpassword,
      newpasswordvalidation,
    } = req.body;

    if (newpassword !== newpasswordvalidation) return res.redirect('/');

    console.log(login);

    const user = await Collections.getCollection('User').findOne({
      login,
    });


    if (!user) return res.redirect('/');

    const {
      salt,
    } = user;

    const passwd = User.methods.crypto(newpassword, salt);

    await Collections.getCollection('User').update({
      login,
      passwd,
    });

    // Send email for each delete user
    Mail.getInstance().update(user.email, new Date());

    return res.redirect('/');
  } catch (e) {
    Logger.logerr(e);

    return res.redirect('/504');
  }
};
