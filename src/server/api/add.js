import Collections from '../collections/collections';
import Logger from '../logger';
import Mail from '../mail';
import SERVERROR from '../errcodes';

module.exports = async (req, res) => {
  const {
    firstname,
    lastname,
    login,
    email,
    passwd,
    passwdValidation,
    admin,
    color,
  } = req.body;

  try {
    try {
      if (passwd !== passwdValidation) throw new Error(SERVERROR.E0300);
    } catch (e) {
      return res.redirect('/add');
    }

    // Check if user already exist zith this login
    const userAlready = await Collections.getCollection('User').findOne({
      login,
    });

    try {
      if (userAlready) throw new Error(SERVERROR.E0301);
    } catch (e) {
      return res.redirect('/add');
    }

    await Collections.getCollection('User').create({
      firstname,

      lastname,

      login,

      passwd,

      email,

      admin,

      color,
    });

    // Trigger mail about add
    setTimeout(() =>
      Mail.getInstance().creation(email, login), 0);

    return res.redirect('/');
  } catch (e) {
    Logger.logerr(e);

    // Should be 505 page
    return res.redirect('/504');
  }
};
