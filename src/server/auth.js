import passport from 'passport';
import LocalStrategy from 'passport-local';

import Collections from './collections/collections';

/**
 * This file controls auth using passport
 */

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  Collections.getCollection('User').findById(
    id,
    (err, user) => done(err, user),
  );
});

const strategy = {
  usernameField: 'user[login]',
  passwordField: 'user[password]',
};

passport.use('local-login', new LocalStrategy(strategy, async (login, password, done) => {
  try {
    const user = await Collections.getCollection('User').findOne({
      login,
    });

    if (!user || !user.validatePassword(password)) {
      return done(null, false, {
        errors: {
          'login or password': 'is invalid',
        },
      });
    }

    return done(null, user);
  } catch (e) {
    return done(e);
  }
}));

export default passport;
