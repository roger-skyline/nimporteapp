import mongoose from 'mongoose';
import crypto from 'crypto';
import fs from 'fs';

import Logger from '../logger';
import Collections from './collections';

const {
  Schema,
} = mongoose;

const User = new Schema({
  firstname: String,

  admin: Boolean,

  lastname: String,

  login: String,

  email: String,

  passwd: String,

  salt: String,

  color: String,
});

User.methods.crypto = (passwd, salt) => {
  const hash = crypto.createHmac('sha512', salt);

  hash.update(passwd);

  return hash.digest('hex');
};

User.methods.salting = () => {
  const SALT_LENGTH = 8;

  return crypto.randomBytes(Math.ceil(SALT_LENGTH / 2))
    .toString('hex')
    .slice(0, SALT_LENGTH);
};

// hashing a password before saving it to the database
User.pre('save', function(next) {
  const salt = User.methods.salting();
  const passwd = User.methods.crypto(this.passwd, salt);

  this.passwd = passwd;
  this.salt = salt;

  next();
});

User.methods.ensureAdmin = async () => {
  // Check if admin has been found
  const user = await Collections.getCollection('User').findOne({
    firstname: 'admin',
    admin: true,
  });

  if (user) {
    Logger.log('Admin exist with login admin, check admin.pwd');
    return;
  }

  const rString = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

  // Create the admin user
  await Collections.getCollection('User').create({
    login: 'admin',
    firstname: 'admin',
    lastname: 'admin',
    email: null,
    admin: true,
    passwd: rString,
    color: `#${Math.floor(Math.random() * 16777215).toString(16)}`,
  });

  fs.writeFileSync('../../admin.pwd', rString);

  Logger.log(`Admin has been created with with login 'admin' and password '${rString}'`);
};

export default User;
