import User from './user';

let instance = null;

export default class Collections {
  constructor() {
    this.collections = {};
  }

  static get collectionSchemas() {
    return {
      User,
    };
  }

  static getInstance() {
    if (!instance) {
      instance = new Collections();
    }

    return instance;
  }

  static loadCollections(db) {
    Collections.getInstance().collections =
      Object.keys(Collections.collectionSchemas).reduce((tmp, x) => {
      tmp[x] = db.model(x, Collections.collectionSchemas[x]);

      return tmp;
    }, {});
  }

  static getCollection(name) {
    return Collections.getInstance().collections[name];
  }
}
