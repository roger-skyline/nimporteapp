import Logger from '../logger';

/**
 * Handle a user login
 */
module.exports = async (req, res) => {
  try {
    if (!req.session.client) return res.redirect('login');

    return res.redirect('/home');
  } catch (e) {
    Logger.logerr(e);

    return res.redirect('/504');
  }
};
