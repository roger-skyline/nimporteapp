import Collections from '../collections/collections';
import Logger from '../logger';

/**
 * Fetch more infos on the user to display it
 * Redirect on / when no user given or no userInfos found (not in DB)
 */
module.exports = async (req, res) => {
  const {
    session,
    query,
  } = req;

  const {
    login,
  } = query;


  try {
    if (!login) return res.redirect('/');

    const userInfos = await Collections.getCollection('User').findOne({
      login,
    });

    if (!userInfos) return res.redirect('/');

    return res.render('udpate', {
      ...session.client,
      user: {
        login: userInfos.login,
        email: userInfos.email,
        lastname: userInfos.lastname,
        firstname: userInfos.firstname,
      },
    });
  } catch (e) {
    Logger.logerr(e);

    // Should be 505 page
    return res.redirect('/504');
  }
};
