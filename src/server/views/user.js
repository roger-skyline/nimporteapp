import Collections from '../collections/collections';
import Logger from '../logger';

/**
 * Handle a user login
 */
module.exports = async (req, res) => {
  try {
    const {
      payload: {
        id,
      },
    } = req;

    const user = await Collections.getCollection('User').findOne({
      _id: id,
    });

    // Check that the provided password matches
    if (!user || String(user.passwd) !== req.body.passwd) return res.redirect('/login');

    return res.render('user', {
      user: user.login,
      color: user.color,
    });
  } catch (e) {
    Logger.logerror(e);

    return res.redirect('/504');
  }
};
