import Collections from '../collections/collections';
import Logger from '../logger';

/**
 * Handle home page
 */
module.exports = async (req, res) => {
  const {
    client,
  } = req.session;

  try {
    if (!client.isAdmin) return res.render('home', client);

    const users = await Collections.getCollection('User').find();

    return res.render('home', {
      ...client,
      users,
    });
  } catch (e) {
    Logger.logerr(e);

    return res.redirect('/504');
  }
};
