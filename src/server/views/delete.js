import Collections from '../collections/collections';
import Logger from '../logger';

module.exports = async (req, res) => {
  const {
    client,
  } = req.session;

  try {
    const users = await Collections.getCollection('User').find({
      login: {
        $ne: 'admin',
      },
    });

    res.render('delete', {
      ...client,
      users,
    });
  } catch (e) {
    Logger.logerr(e);

    // Should be 505 page
    res.redirect('/504', client);
  }
};
