export default {
  // From E100 to E200, all errors about reading env.json file
  E0100: 'The "server" is missing from configuration file',
  E0101: 'The configuration file is missing',
  E0102: 'The "db" is missing from configuration file',

  // From E200 to E300, erros abot collection creation / instancitation / etc
  E0200: 'The collection is missing the collectionName implementation',
  E0201: 'The collection is missing the schema implementation',

  // From E300 to E400, errors user account creation (add)
  E0300: 'Password validation missmatch',
  E0301: 'User already exist with this login',
};
