#!/bin/bash

openssl req -newkey rsa:2048 -new -nodes -keyout server.pem -out server.pem
openssl x509 -req -days 365 -in server.pem -signkey server.pem -out server.crt
