"use strict";

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    babel: {
      options: {
        sourceMap: true,
      },
      files: {
        expand: true,
        src: ['./src/**/*.js'],
        ext: '.js',
        dest: './build/',
      },
    },

    copy: {
      main: {
        files: [
          {
            expand: true,
            cwd: 'src/views',
            src: '**',
            dest: './build/views',
          },
          {
            expand: true,
            cwd: 'sslcerts',
            src: '**',
            dest: './build/sslcerts',
          },
        ],
      },
    },
  });

  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.registerTask('default', ['newer:babel', 'copy:main']);
};
